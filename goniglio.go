package main

// This program prints all the DNS responses received by the host it runs on,
// and prints a warning if a reply is not coming from TRUSTED_DNS_IP.
// WARNING: it does not intercept outgoing traffic, nor traffic directed to
//          other hosts. Use pcap or eBPF for that.

import (
    "os"
    "fmt"
    "net"
    "log"
    "flag"
    "sync"
    "time"
    "bufio"
    "strconv"
    "strings"
    "github.com/fatih/color"
    "github.com/google/gopacket"
    "github.com/google/gopacket/pcap"
    "github.com/google/gopacket/layers"
    "github.com/fsnotify/fsnotify"
)

type Sniffer struct {
    *gopacket.PacketSource
    handle *pcap.Handle
}

func (s *Sniffer) CleanUp() {
    if s.handle != nil {
        s.handle.Close()
    }
}

func get_sniffer(iface, bpffilter string, snaplen int, promisc bool) (*Sniffer, error) {
    var (
        sniffer *gopacket.PacketSource
        handle *pcap.Handle
        err error
    )
    if iface == "any" && promisc {
        color.Set(color.FgYellow)
        log.Println("Warning: promiscuous mode is not supported on the `any` interface, disabling promiscuous mode. See https://github.com/the-tcpdump-group/tcpdump/issues/480#issuecomment-138317953\n")
        color.Unset()
        promisc = false
    }
    if bpffilter == "" {
        handle, err = pcap.OpenLive(iface, int32(snaplen), promisc, time.Second)
        if err != nil {
            return nil, err
        }
    } else {
        inactive, err := pcap.NewInactiveHandle(iface)
        if err != nil {
            return nil, err
        }
        defer inactive.CleanUp()
        if err = inactive.SetSnapLen(int(snaplen)); err != nil {
            return nil, err
        }
        if err = inactive.SetPromisc(promisc); err != nil {
            return nil, err
        }
        if err = inactive.SetTimeout(time.Second); err != nil {
            return nil, err
        }
        if handle, err = inactive.Activate(); err != nil {
            return nil, err
        }
        if bpffilter != "" {
            if err = handle.SetBPFFilter(bpffilter); err != nil {
                return nil, err
            }
        }
    }

    sniffer = gopacket.NewPacketSource(handle, handle.LinkType())
    sniffer.Lazy = true  // do lazy decoding
    sniffer.NoCopy = true  // do not copy packets - but don't modify them either!
    return &Sniffer{sniffer, handle}, nil
}


var resolvers *[]string
var once sync.Once

func parseSystemResolvers() *[]string {
    file, err := os.Open("/etc/resolv.conf")
    if err != nil {
        log.Fatalln(err)
    }
    defer file.Close()

    tmpResolvers := make([]string, 0)
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        line := scanner.Text()
        if strings.HasPrefix(line, "nameserver ") {
            sl := strings.Split(line, " ")
            if len(sl) < 2 {
                // skip line
                log.Printf("Invalid nameserver line in /etc/resolv.conf: %v\n", line)
                continue
            }
            resolver := net.ParseIP(sl[1])
            if resolver == nil {
                log.Printf("Invalid resolver IP: %v\n", sl[1])
            }
            tmpResolvers = append(tmpResolvers, resolver.String())
        }
    }
    return &tmpResolvers
}

func setupResolverWatcher() (*fsnotify.Watcher, error) {
    // set up auto-updates
    watcher, err := fsnotify.NewWatcher()
    if err != nil {
        return nil, err
    }
    go func() {
        for {
            select {
                case event := <-watcher.Events:
                    if event.Op & fsnotify.Write == fsnotify.Write {
                        color.Set(color.FgRed)
                        log.Print("Detected changes to /etc/resolv.conf, updating resolvers")
                        resolvers = parseSystemResolvers()
                        log.Printf("Updated system resolvers: %v", *resolvers)
                        color.Unset()
                    }
                case err := <-watcher.Errors:
                    log.Printf("File watcher error for /etc/resolv.conf: %v", err)
            }
        }
    }()
    err = watcher.Add("/etc/resolv.conf")
    if err != nil {
        return nil, err
    }
    return watcher, nil
}

func getSystemResolvers() *[]string {
    // hacky function to return a list of nameservers from /etc/resolv.conf .
    // unfortunately net.systemConf is not exposed and I can't use that.
    // Works only on systems that use and honour resolv.conf .
    once.Do(func() {
        resolvers = parseSystemResolvers()
        log.Printf("Updated system resolvers: %v", resolvers)
    })
    return resolvers
}

func isResolverIP(ip string) bool {
    for _, resolver := range *getSystemResolvers() {
        if ip == resolver {
            return true
        }
    }
    return false
}

type Flow struct {
    srcIP string
    srcPort uint16
    dstIP string
    dstPort uint16
}

type DNSCryptListener struct {
    ip string
    port uint16
}
var dnscryptlistener DNSCryptListener

func isGoingThroughDNSCrypt(flow Flow) bool {
    return (flow.dstIP == dnscryptlistener.ip && flow.dstPort == dnscryptlistener.port) ||
        (flow.srcIP == dnscryptlistener.ip && flow.srcPort == dnscryptlistener.port)
}

func fmtRRs(shortname string, rrs []layers.DNSResourceRecord) string {
    if len(rrs) == 0 {
        return ""
    }
    formatted := fmt.Sprintf("%s: [", shortname)
    for i := 0; i < len(rrs); i++ {
        formatted += rrs[i].String()
        if i < len(rrs) - 1 {
            formatted += ", "
        }
    }
    formatted += "] "
    return formatted
}

var (
    iface *string = flag.String("interface", "lo", "The interface to sniff packets from")
    snaplen *int = flag.Int("snaplen", 65535, "Number of bytes to snarf from each packet")
    promisc *bool = flag.Bool("promisc", true, "Enable or disable promiscuous mode")
    dump *bool = flag.Bool("dump", false, "Dump full packet data")
    dnscrypt_ip_port *string = flag.String("dnscrypt", "", "IP and port of your dnscrypt instance, separated by colon. Example: 127.0.2.1:5353. Default: none")
)

func main() {
    flag.Parse()
    watcher, err := setupResolverWatcher()
    if err != nil {
        log.Fatalf("Error settin up watcher for /etc/resolv.conf: %v", err)
    }
    defer watcher.Close()

    bpffilter := "udp port 53" // only UDP for now
    if *dnscrypt_ip_port != "" {
        tmp := strings.Split(*dnscrypt_ip_port, ":")
        if len(tmp) != 2 {
            log.Fatalf("Invalid ip:port format: %v", *dnscrypt_ip_port)
        }
        if ip_inst := net.ParseIP(tmp[0]); ip_inst == nil {
            log.Fatalf("Invalid IP address: %v", tmp[0])
        }
        dnscryptlistener.ip = tmp[0]
        port_int, err := strconv.Atoi(tmp[1])
        if err != nil {
            log.Fatalf("Invalid port: %v", tmp[1])
        }
        if port_int < 1 || port_int > 65535 {
            log.Fatalf("Invalid port: out of range [1, 65535]: %d", port_int)
        }
        dnscryptlistener.port = uint16(port_int)
        log.Printf("DNSCrypt instance is at %s:%d", dnscryptlistener.ip, dnscryptlistener.port)
    } else {
        log.Print("No DNSCrypt instance specified")
    }
    sniffer, err := get_sniffer(*iface, bpffilter, *snaplen, *promisc)
    defer sniffer.CleanUp()
    if err != nil {
        log.Fatalf("Error: %v\n", err)
    }

    fmt.Printf("Sniffing on interface `%s`, snaplen=%d, promisc=%v, BPF filter \"%s\"\n",
        *iface, *snaplen, *promisc, bpffilter)
    fmt.Printf("System resolvers: %v\n", *getSystemResolvers())
    for packet := range sniffer.Packets() {
        t := time.Now().Format("[2006-01-02 15:04:05 -0700] ")
        color.Set(color.FgYellow)
        fmt.Print(t)
        color.Unset()
        var (
            srcIP, dstIP net.IP
            srcPort, dstPort layers.UDPPort
        )
        for _, layer := range packet.Layers() {
            if layer.LayerType() == layers.LayerTypeIPv4 {
                ipLayer := packet.Layer(layers.LayerTypeIPv4)
                ip := ipLayer.(*layers.IPv4)
                srcIP, dstIP = ip.SrcIP, ip.DstIP
            } else if layer.LayerType() == layers.LayerTypeIPv6 {
                ipLayer := packet.Layer(layers.LayerTypeIPv6)
                ip := ipLayer.(*layers.IPv6)
                srcIP, dstIP = ip.SrcIP, ip.DstIP
            } else if layer.LayerType() == layers.LayerTypeUDP {
                udpLayer := packet.Layer(layers.LayerTypeUDP)
                udp := udpLayer.(*layers.UDP)
                srcPort, dstPort = udp.SrcPort, udp.DstPort
            } else if layer.LayerType() == layers.LayerTypeDNS {
                dnsLayer := packet.Layer(layers.LayerTypeDNS)
                dns := dnsLayer.(*layers.DNS)

                flow := Flow{
                    srcIP: srcIP.String(),
                    srcPort: uint16(srcPort),
                    dstIP: dstIP.String(),
                    dstPort: uint16(dstPort),
                }
                // Use green if going through the system resolver, yellow if not
                // going through dnscrypt, red if not going through the system
                // resolver
                // FIXME handle ports different from 53 in resolv.conf
                // TODO use struct Flow for the resolver IP check
                if (srcPort == 53 && !isResolverIP(flow.srcIP)) || (flow.dstPort == 53 && !isResolverIP(flow.dstIP)) {
                    if !isGoingThroughDNSCrypt(flow) {
                        color.Set(color.FgRed)
                        fmt.Printf("[W] ")
                    } else {
                        // going through dnscrypt, all good!
                        color.Set(color.FgGreen)
                    }
                } else {
                    color.Set(color.FgGreen)
                }
                // print IP information
                fmt.Printf("%s:%d -> %s:%d ", srcIP, srcPort, dstIP, dstPort)
                color.Unset()

                // print DNS information
                if !dns.QR {
                    fmt.Printf("QUESTION ")
                } else {
                    fmt.Printf("RESPONSE ")
                }
                // Questions
                fmt.Print("Q: [")
                for i := 0; i < len(dns.Questions); i++ {
                    rr := dns.Questions[i]
                    fmt.Printf("%s %s %v", rr.Class, rr.Type, string(rr.Name))
                    if i < len(dns.Questions) - 1 {
                        fmt.Print(", ")
                    }
                }
                fmt.Print("] ")
                // Answers, Authorities, Additionals
                fmt.Print(fmtRRs("Answers", dns.Answers))
                fmt.Print(fmtRRs("Authorities", dns.Authorities))
                fmt.Println(fmtRRs("Additionals", dns.Additionals))
            }
        }
        if *dump {
            fmt.Println(packet.Dump())
        }
    }
}
