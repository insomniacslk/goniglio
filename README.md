# Goniglio

Goniglio is a basic DNS packet sniffer written in Go. If the DNS traffic doesn't go through the system resolver, the output will have some alarming red.

# Requirements
 go 1.6

# How to use
* `go build`
* `sudo ./goniglio -interface any` # Read the code before running it with sudo, you won't really trust me, right?

Full help:

```
$ ./goniglio -h
Usage of ./goniglio:
  -dnscrypt string
    	IP and port of your dnscrypt instance, separated by colon. Example: 127.0.2.1:5353. Default: none
  -dump
    	Dump full packet data
  -interface string
    	The interface to sniff packets from (default "lo")
  -promisc
    	Enable or disable promiscuous mode (default true)
  -snaplen int
    	Number of bytes to snarf from each packet (default 65535)
```


## Example

![goniglio_session](res/goniglio_session.png)

## Why the name?

Go + coniglio (italian for "bunny"). Bunny? Well, it's a sniffer:

![goniglio](res/goniglio.gif)